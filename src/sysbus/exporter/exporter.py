#! /usr/bin/env python3

from sysbus import sysbus
from datetime import datetime
import time
import sys
    
def get_dsl_metrics():

    # load config from ~/.sysbusrc
    sysbus.load_conf()
    sysbus.auth()
    r = sysbus.requete("NeMo.Intf.dsl0:getMIBs", { "traverse": "this" })['status']['dslline']['dsl0']
    dsl_metrics = {
            'status': True if r['LineStatus'] == 'Up' else False,
            'upstream_rate_max': r['Line_UpstreamMaxRate'],
            'downstream_rate_max': r['Line_DownstreamMaxRate'],
            'downstream_noise_margin': r['Line_DownstreamNoiseMargin'],
            'upstream_noise_margin': r['Line_UpstreamNoiseMargin'],
            'upstream_current_rate': r['Line_UpstreamCurrRate'],
            'downstream_current_rate': r['Line_DownstreamCurrRate']
            }
    return dsl_metrics

if __name__ == '__main__':
    while True:
        time.sleep(30)
        date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        date = date_time.split(' ')[0]
        with open("{}_livebox.csv".format(date), "a") as f:
        
            try:
                metrics = get_dsl_metrics()
                data = "{};{};{};{};{};{}".format(date_time, metrics['status'],metrics['downstream_current_rate'],metrics['upstream_current_rate'],metrics['downstream_noise_margin'],metrics['upstream_noise_margin'])
            except:
                data = "{};False;-1;-1;-1;-1".format(date_time)
            print(data)
            f.write(data + "\n")
